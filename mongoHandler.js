//import declarations
const mongoDB = require('mongodb').MongoClient;

//const declaration
const URL = "mongodb://localhost:27017";
const dbName = "SampleDB";
let dB;

let mongoHandler = {};

//mongoDB connection
module.exports = connect = mongoDB.connect(URL, (err, client) => {
  if (err) console.log(err);

  dB = client.db(dbName);
  console.log(`Connected to the server successfully`)
})

//get function
mongoHandler.get = function (tableName, data, callback) {
  const collection = dB.collection(tableName);
  collection.findOne(
    data,
    function (err, result) {
      if (err) console.error(err);
      callback(result);
    }
  );
};

//This function is used to post a document
mongoHandler.post = function (tableName, data, callback) {
  dB.collection(tableName).insertOne(data, function (err, result) {
    if (err) console.log(err);
    callback(result);
  });
};

//This function is used to put a document
mongoHandler.put = function (tableName, query, data, callback) {
  dB.collection(tableName).updateOne(
    query,
    { $set: data },
    { upsert: false },
    (err, res) => {
      if (err) callback(err);
      callback(res);
    }
  );
};

module.exports = mongoHandler;