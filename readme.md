Pre-requisites

         1) Postman. Will need postman installed to get API
          The API is linked below

          http://localhost:3005/welcome

          The counter has been made public and will be returned in API for representational purposes.

          2) MongoCompass
          To verify the counter in payload as well as database.
          SampleDB > hitsCounter > counterObj

          3) Visual studio code (optional but would be great)

         Run steps
         1) npm i.
         2) Run npm run start in terminal.
         3) Open postman and hit API
         4) Check in mongo compass