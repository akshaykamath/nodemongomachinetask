const handler = require("./mongoHandler");

mainComponent = {};

mainComponent.welcome = function (headers, callback) {
  let counterExists;
  handler.get("hitsCounter", { api: headers.trimmedPath }, (data) => {

    counterExists = data;

    posterMethod(headers, counterExists);

    let hitCounter =
      (counterExists === null) | undefined ? 1 : counterExists.counter + 1;
    callback(200, {
      name: "Akshay",
      hobby: "Driving",
      message: "Welcome, you have hit the welcome method",
      hitCounter: hitCounter,
    });
  });
};

function posterMethod(headers, counterDoc) {
  let counter = 0;
  if (counterDoc === null) {
    counter = 1;
    handler.post(
      "hitsCounter",
      { counter: counter, api: headers.trimmedPath },
      (_data) => {}
    );
  } else {
    counter = counterDoc.counter + 1;
    handler.put(
      "hitsCounter",
      { api: headers.trimmedPath },
      { counter: counter },
      (_data) => {}
    );
  }
}

module.exports = mainComponent;
