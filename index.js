var http = require("http");
const url = require("url");
var StringDecoder = require("string_decoder").StringDecoder;

var handler = require("./mongoHandler");
var mainComponent = require("./mainPage");

const PORT = "3005";

const server = http.createServer((req, res) => {
  myServer(req, res);
});

server.listen(PORT, () => {
  console.log(`HTTP server is listening on port ${PORT}`);
});

var myServer = function (req, res) {
  // Parse the url
  var parsedUrl = url.parse(req.url, true);
  var queryStringObject = parsedUrl.query;
  // Get the path
  var path = parsedUrl.pathname;
  var trimmedPath = path.replace(/^\/+|\/+$/g, "");

  var method = req.method;

  var decoder = new StringDecoder("utf-8");
  var buffer = "";

  var headers = req.headers;

  req.on("data", function (data) {
    buffer += decoder.write(data);
  });

  req.on("end", function () {
    buffer += decoder.end();
    // Check the router for a matching path for a handler. If one is not found, use the notFound handler instead.
    var chosenHandler = mainComponent[trimmedPath];
    // Construct the data object to send to the handler
    var data = {
      trimmedPath: trimmedPath,
      queryStringObject: queryStringObject,
      method: method,
      headers: headers,
      payload: buffer,
    };

    // Route the request to the handler specified in the router
    chosenHandler(data, function (statusCode, payload) {
      // Use the status code returned from the handler, or set the default status code to 200
      statusCode = typeof statusCode == "number" ? statusCode : 200;
      // Use the payload returned from the handler, or set the default payload to an empty object
      payload = typeof payload == "object" ? payload : {};
      // Convert the payload to a string
      var payloadString = JSON.stringify(payload);
      // Return the response
      res.setHeader("Content-Type", "application/json");
      res.writeHead(statusCode);
      res.end(payloadString);
    });
  });
};
